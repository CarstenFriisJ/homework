#define ALG gsl_multimin_fminimizer_nmsimplex2
#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multimin.h>
#include <tgmath.h>
#include <gsl/gsl_integration.h>

struct exp_data{int n; double *t,*y,*e;}; //n is number, t,y,e are arrays, therefor they are pointers.


double min_function(const gsl_vector  *x, void* params){ 
	double A=gsl_vector_get(x,0);// these are the variables we want to minimize
	double T=gsl_vector_get(x,1);
	double B=gsl_vector_get(x,2);

	struct exp_data *p = (struct exp_data*) params; //parsing data in params.
	int n=(*p).n;
	double *t=(*p).t;
	double *y=(*p).y;
	double *e=(*p).e;

	double sum = 0;
	for(int i=0; i<n;i++) {
		sum = sum + pow((A*exp(-t[i]/T)+B)-y[i],2)/pow(e[i],2);
	}

	return sum;
}

int main(){
	double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
	double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
	double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09}; 
	int n = sizeof(t)/sizeof(t[0]);

	printf("# t[i], y[i], e[i]\n");
	for (int i=0; i<n;i++){
		printf("%g %g %g \n", t[i],y[i],e[i]); //We use this to plot error bars. Can reference this with index 0 in plot.gpi
	}
	printf("\n\n");

	struct exp_data pa; //making structure for data
	pa.n=n;
	pa.t=t;
	pa.y=y;
	pa.e=e;

	size_t dim=3;
	gsl_multimin_function F;
	F.f=min_function;
	F.n=dim;
	F.params=(void*)&pa; //setting out structure to be parameters


	gsl_multimin_fminimizer * state =
	gsl_multimin_fminimizer_alloc (ALG,dim);
	gsl_vector *start = gsl_vector_alloc(dim);
	gsl_vector *step = gsl_vector_alloc(dim);
	gsl_vector_set(start,0,3); /* A_start */
	gsl_vector_set(start,1,1); /* T_start */
	gsl_vector_set(start,2,1); /* B_start */
	gsl_vector_set_all(step,1);// setting all variables o use same step.
	gsl_multimin_fminimizer_set (state, &F, start, step);


	int iter=0, status;
	double acc = 0.01;
	do{
		iter++;
		int flag = gsl_multimin_fminimizer_iterate (state);
		if(flag!=0)break;
		status = gsl_multimin_test_size (state->size, acc);
		if (status == GSL_SUCCESS) fprintf (stderr,"converged\n");
		fprintf(stderr,
			"iter=%2i, A= %2f, T= %2f, B= %2g, size= %2g, fval/n= %2f\n",
			iter,
			gsl_vector_get((*state).x,0),
			gsl_vector_get((*state).x,1),
			gsl_vector_get((*state).x,2),
			(*state).size,
			(*state).fval);
	}while(status == GSL_CONTINUE && iter < 99);
	//Now we want to print the values from the fitted function to plot it.
	double A= gsl_vector_get((*state).x,0);
	double T= gsl_vector_get((*state).x,1);
	double B= gsl_vector_get((*state).x,2);
	//Have to make for loop for values t(0) to t(n-1), as n is the size.
	double dt = (t[n-1]-t[0])/50;
	printf("# t, A*exp(-t/T)+B\n");
	for( double i=t[0]; i<t[n-1];i+=dt){
		printf("%g %g \n",i,A*exp(-i/T)+B);
	}
	gsl_vector_free(start);
	gsl_vector_free(step);
	gsl_multimin_fminimizer_free(state);
	return 0;
}
