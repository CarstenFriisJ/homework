#include <gsl/gsl_odeiv2.h>
#include <gsl/gsl_errno.h>
#include <stdio.h>
#include <tgmath.h>
#include <assert.h>

int diff_eq(
	double r, const double y[], double dydr[], void * params){
	double epsilon = *(double *)params;
	dydr[0]=y[1];
	dydr[1]= -2*y[0]*(1/r+epsilon);
return GSL_SUCCESS;
}



double energy(double epsilon,double r){
	assert(r>=0);
	const double rmin=1e-3;
	if(r<rmin) {
		return r=r-r*r;
	}
	gsl_odeiv2_system sys;
	sys.function = diff_eq;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void *)&epsilon;

	double acc=1e-6,eps=1e-6,hstart=0.01;
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new
		(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

	double t=rmin,y[2]={t-t*t,1-2*t}; //Starting at t=rmin as the if condition takes care of lower values.
	gsl_odeiv2_driver_apply(driver,&t,r,y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

