#include <stdio.h>
#include <tgmath.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_multiroots.h>
#define ALG gsl_multiroot_fsolver_hybrids


int master(const gsl_vector * x, void *params, gsl_vector * f){
	params = NULL;
	double x0=gsl_vector_get(x,0);
	double x1=gsl_vector_get(x,1);

	double y0=(1-x0)*(1-x0);
	double y1=100*(x1-x0*x0)*(x1-x0*x0);
	gsl_vector_set(f,0,y0);
	gsl_vector_set(f,1,y1);
	return GSL_SUCCESS;
}


int main()
{
	const int dim= 2;
	gsl_multiroot_function M;
	M.f = master;
	M.n=dim;
	M.params = NULL;

	gsl_multiroot_fsolver *s = gsl_multiroot_fsolver_alloc (ALG, dim);
	gsl_vector * x = gsl_vector_alloc(dim);
	gsl_vector_set(x,0,1.5);
	gsl_vector_set(x,1,1.5);
	gsl_multiroot_fsolver_set (s, &M, x);

	int iter=0,status;
	const double acc=1e-12;
	do{
		iter++;
		int flag=gsl_multiroot_fsolver_iterate(s);
		if(flag!=0)break;
		status = gsl_multiroot_test_residual((*s).f,acc);
		fprintf(stderr," iter=%2i, x=%4g,y=%4g, f=%4g\n"
			,iter
			,gsl_vector_get((*s).x,0)
			,gsl_vector_get((*s).x,1)
			,gsl_vector_get((*s).f,0));
	}while(status== GSL_CONTINUE && iter<200);

	gsl_vector_free(x);
	gsl_multiroot_fsolver_free(s);
	return 0;
}

