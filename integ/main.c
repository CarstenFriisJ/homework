#include <stdio.h>
#include <tgmath.h>

double variational_energy();

int main(){
	double a=0.1,b=10, dx=0.0001;
	for(double x=a;x<b;x+=dx)
		printf("%g %g\n",x,variational_energy(x));
	return 0;
}

