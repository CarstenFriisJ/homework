#include <tgmath.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_errno.h>

double integrand(double x, void*params){
	params = NULL;
	return log(x)/sqrt(x);
}

double function(double x){
	gsl_function f;
	f.function = integrand;

	int limit = 100;
	double a=0,acc=1e-6,eps1e-6,result,err;
	gsl_integration_workspace * workspace =
		gsl_integration_workspace_alloc(limit);
	int status=gsl_integration_qag(
		&f,a,acc,eps,limit,workspace,&result,&err);
	gsl_integration_workspace_free(workspace);
	if(status!=GSL_SUCCES) return NAN;
	else return result;
}