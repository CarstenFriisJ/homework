#include <math.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_errno.h>

double integrand_psi(double x, void*params){
	double a = *(double*)params;
	return exp(-a*pow(x,2));
}

double integrand_H(double x, void*params){
	double a = *(double*)params;
	return (-pow(a,2)*pow(x,2)/2+a/2+pow(x,2)/2)*exp(-a*pow(x,2));
}

double variational_integral(double a, double integrand(double x, void*params)){
	gsl_function f;
	f.function = integrand;
	f.params = (void*)&a;
	int limit = 100;
	double acc=1e-6,eps=1e-6,result,err;
	gsl_integration_workspace * workspace =
		gsl_integration_workspace_alloc(limit);
	int status=gsl_integration_qagi
		(&f,acc,eps,limit,workspace,&result,&err);
	gsl_integration_workspace_free(workspace);
	if(status!=GSL_SUCCESS) return NAN;
	else return result;
}


double variational_energy(double a) {
	return variational_integral(a, integrand_H)/variational_integral(a, integrand_psi);
}